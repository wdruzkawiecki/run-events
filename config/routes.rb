Rails.application.routes.draw do
  root to: "home#index"

  devise_for :users, path: "users"
  devise_for :admins, path: "admins"

  namespace :admin do
    resources :events
  end

  resources :events, only: [:show] do
    resources :participations, only: [:create]
    resources :comments, only: [:create]
  end

  resources :users, only: [:show]
end
