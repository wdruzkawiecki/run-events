class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :participations
  has_many :events, through: :participations
  has_many :comments

  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :birthdate, presence: true
end
