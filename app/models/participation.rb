class Participation < ApplicationRecord
  AGE_CATEGORIES = %w( G16 G20 G30 G40 G50 G60 G70 ).freeze

  belongs_to :user
  belongs_to :event

  validates :user, presence: true, uniqueness: { scope: :event }
  validates :event, presence: true
  validates :age_category, presence: true,
    inclusion: { in: AGE_CATEGORIES,
      message: "is not a valid age age_category" }
  validates :start_number, presence: true
end
