class Event < ApplicationRecord
  after_update :notify

  belongs_to :admin
  has_many :participations
  has_many :users, through: :participations
  has_many :comments

  validates :name, presence: true
  validates :description, presence: true
  validates :start_at, presence: true
  validates :slots,
    numericality: { greater_than: 0, only_integer: true },
    presence: true
  validates :city, presence: true
  validates :country_id, presence: true
  validates :distance,
    numericality: { greater_than: 0, only_integer: true },
    presence: true
  validate :participations_count

  scope :upcoming, -> { where("start_at >= ?", Date.current) }

  def participations_count
    errors.add(:tags, "too much participations") if participations.size >= slots
  end

  def notify
    users.each do |user|
      EventMailFactory.build(self, user)
    end
  end
end
