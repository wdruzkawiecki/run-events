class UserFacade
  attr_reader :user

  def initialize(user)
    @user = user
  end

  def recent_events
    @user.events.last(3)
  end

  def recent_comments
    @user.comments.last(3)
  end
end
