class EventMailer < ApplicationMailer
  default from: 'notifications@example.com'

  def send_description_changed(user)
    @user = user
    mail(to: @user.email, subject: 'Description has changed in your upcoming event')
  end

  def send_start_at_changed(user)
    @user = user
    mail(to: @user.email, subject: 'Check your event, because start_at changed!')
  end
end
