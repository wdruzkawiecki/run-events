class EventMailFactory
  def self.build(event, user)
    mailer = EventMailer.new
    if event.description_changed?
      mailer.send_description_changed(user)
    elsif event.start_at_changed?
      mailer.send_start_at_changed(user)
    end
  end
end
