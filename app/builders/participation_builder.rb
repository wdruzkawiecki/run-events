class ParticipationBuilder
  attr_reader :participation

  def initialize
    @participation = Participation.new
  end

  def add_user(user)
    @participation.user = user
  end

  def add_event(event)
    @participation.event = event
  end

  def configure
    set_start_number
    set_age_category
  end

  private

  def set_age_category
    if @participation.event
      number = @participation.event.users.size + 1
      @participation.start_number = number
    end
  end

  def set_start_number
    if user = @participation.user
      age = (Date.today - user.birthdate).to_i / 365
      @participation.age_category =
        case age
        when 16..19
          "G16"
        when 20..29
          "G20"
        when 30..39
          "G30"
        when 40..49
          "G40"
        when 50..59
          "G50"
        when 60..69
          "G60"
        when age >= 70
          "G70"
        end
    end
  end
end
