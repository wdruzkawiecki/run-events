class EventDecorator < Draper::Decorator
  delegate_all

  def location
    "#{object.city}, #{country_name}"
  end

  def formatted_start_at
    object.start_at.strftime("%d %b, %Y")
  end

  def formatted_distance
    h.number_to_human(object.distance, units: :distance)
  end

  private

  def country_name
    ISO3166::Country[object.country_id]
  end
end
