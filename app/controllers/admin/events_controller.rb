class Admin::EventsController < Admin::AdminController
  before_action :set_event, only: [:edit, :update, :destroy]

  def index
    @events = current_admin.events.decorate
  end

  def new
    @event = current_admin.events.build
  end

  def create
    @event = current_admin.events.build(event_params)

    if @event.save
      redirect_to admin_events_path, notice: "Event was successfully created."
    else
      render :new
    end
  end

  def edit
  end

  def update
    if @event.update(event_params)
      redirect_to admin_events_path, notice: "Event was successfully updated."
    else
      render :edit
    end
  end

  def destroy
    @event.destroy

    if @event.destroyed?
      redirect_to admin_events_path, notice: "Event was successfully destroyed."
    else
      redirect_to admin_events_path, alert: "Something went wrong."
    end
  end

  private

  def event_params
    params.require(:event).permit(:name, :description, :country_id, :city,
      :slots, :start_at, :distance)
  end

  def set_event
    @event = current_admin.events.find(params[:id])
  end
end
