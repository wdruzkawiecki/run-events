class EventsController < ApplicationController
  def show
    @event = Event.find(params[:id]).decorate
    @comment = Comment.new
  end
end
