class HomeController < ApplicationController
  def index
    @events = Event.upcoming.decorate
  end
end
