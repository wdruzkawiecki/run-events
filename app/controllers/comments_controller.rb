class CommentsController < ApplicationController
  before_action :authenticate_user!

  def create
    event = Event.find(params[:event_id])
    @comment = current_user.comments.build(comment_params.merge(event: event))

    if @comment.save
      redirect_to event_path(event)
    else
      redirect_to event_path(event), alert: "Something went wrong"
    end
  end

  private

  def comment_params
    params.require(:comment).permit(:content)
  end
end
