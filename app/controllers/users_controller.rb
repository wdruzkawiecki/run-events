class UsersController < ApplicationController
  def show
    user = User.find(params[:id])
    @user = UserFacade.new(user)
  end
end
