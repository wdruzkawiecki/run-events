class ParticipationsController < ApplicationController
  before_action :authenticate_user!

  def create
    event = Event.find(params[:event_id])
    builder = ParticipationBuilder.new
    builder.add_event(event)
    builder.add_user(current_user)
    builder.configure

    if builder.participation.save
      redirect_to event_path(event),
        notice: "You successfully joined to the event"
    end
  end
end
