class CreateEvents < ActiveRecord::Migration[5.1]
  def change
    create_table :events do |t|
      t.string :name
      t.text :description
      t.string :country_id
      t.string :city
      t.integer :distance
      t.date :start_at
      t.integer :slots
      t.references :admin, foreign_key: true

      t.timestamps
    end
  end
end
